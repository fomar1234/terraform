provider "aws" {
    region = "us-east-1"
}

variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}

resource "aws_vpc" "myapp-vpc" {
    cidr_block =  var.vpc_cidr_block
    tags = {
        Name = "${var.env_prefix}-vpc-1"
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.vpc_cidr_block
    availability_zone = var.avail_zone
        tags = {
        Name = "${var.env_prefix}-subnet-1"
    }
}